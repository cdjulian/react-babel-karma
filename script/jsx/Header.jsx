export default React.createClass({
    render: function() {
        return (
            <h1>
                <a href="#">{this.props.title}</a>
            </h1>
        );
    }
});