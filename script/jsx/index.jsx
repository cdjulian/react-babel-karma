import Header from './Header.jsx';
import WidgetList from './WidgetList.jsx';
React.render(<Header title='React + Webpack + Karma'/>, document.getElementById('header'));
React.render(<WidgetList/>, document.getElementById('content'));