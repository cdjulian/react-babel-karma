import Widget from './Widget.jsx';

export default React.createClass({
    render: function() {
        return (
            <div>
                <Widget type="Foo"/>
                <Widget type="Bar"/>
                <Widget type="Baz"/>
            </div>
        );
    }
});