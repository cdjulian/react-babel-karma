export default React.createClass({
    render: function() {
        return (
            <div>I am a {this.props.type} widget</div>
        );
    }
});