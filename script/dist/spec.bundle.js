/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(4);
	module.exports = __webpack_require__(5);


/***/ },
/* 1 */,
/* 2 */,
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports["default"] = React.createClass({
	    displayName: "Widget",
	
	    render: function render() {
	        return React.createElement("div", null, "I am a ", this.props.type, " widget");
	    }
	});
	module.exports = exports["default"];

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	// set fixtures
	'use strict';
	
	jasmine.getFixtures().fixturesPath = 'base/script/spec/fixtures';
	
	// programatically dispatch mouse events
	function dispatchMouseEvent(el, type, x, y) {
	    if (typeof MouseEvent == 'function') {
	        var e = new MouseEvent(type, {
	            bubbles: true,
	            cancelable: true,
	            view: window,
	            screenX: x,
	            screenY: y,
	            clientX: x,
	            clientY: y
	        });
	        el.dispatchEvent(e);
	    } else {
	        var e2 = document.createEvent('MouseEvents');
	        e2.initMouseEvent(type, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
	        el.dispatchEvent(e2);
	    }
	}

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _jsxWidgetJsx = __webpack_require__(3);
	
	var _jsxWidgetJsx2 = _interopRequireDefault(_jsxWidgetJsx);
	
	describe('Widget', function () {
	    var component;
	
	    beforeEach(function () {
	        loadFixtures('WidgetFixture.html');
	        var container = document.getElementById('example');
	
	        var element = React.createElement(_jsxWidgetJsx2['default']);
	        component = React.render(element, container);
	    });
	
	    it('should render', function () {
	        expect(component.getDOMNode()).toExist();
	    });
	});

/***/ }
/******/ ]);
//# sourceMappingURL=spec.bundle.js.map