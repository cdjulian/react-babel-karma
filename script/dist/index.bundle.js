/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }
	
	var _HeaderJsx = __webpack_require__(1);
	
	var _HeaderJsx2 = _interopRequireDefault(_HeaderJsx);
	
	var _WidgetListJsx = __webpack_require__(2);
	
	var _WidgetListJsx2 = _interopRequireDefault(_WidgetListJsx);
	
	React.render(React.createElement(_HeaderJsx2['default'], { title: 'React + Webpack + Karma' }), document.getElementById('header'));
	React.render(React.createElement(_WidgetListJsx2['default'], null), document.getElementById('content'));

/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports["default"] = React.createClass({
	    displayName: "Header",
	
	    render: function render() {
	        return React.createElement("h1", null, React.createElement("a", { href: "#" }, this.props.title));
	    }
	});
	module.exports = exports["default"];

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
	
	var _WidgetJsx = __webpack_require__(3);
	
	var _WidgetJsx2 = _interopRequireDefault(_WidgetJsx);
	
	exports["default"] = React.createClass({
	    displayName: "WidgetList",
	
	    render: function render() {
	        return React.createElement("div", null, React.createElement(_WidgetJsx2["default"], { type: "Foo" }), React.createElement(_WidgetJsx2["default"], { type: "Bar" }), React.createElement(_WidgetJsx2["default"], { type: "Baz" }));
	    }
	});
	module.exports = exports["default"];

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	
	exports.__esModule = true;
	exports["default"] = React.createClass({
	    displayName: "Widget",
	
	    render: function render() {
	        return React.createElement("div", null, "I am a ", this.props.type, " widget");
	    }
	});
	module.exports = exports["default"];

/***/ }
/******/ ]);
//# sourceMappingURL=index.bundle.js.map