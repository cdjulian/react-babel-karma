import Widget from '../jsx/Widget.jsx';

describe('Widget', function() {
    var component;

    beforeEach(function() {
        loadFixtures('WidgetFixture.html');
        var container = document.getElementById('example');

        var element = React.createElement(Widget);
        component = React.render(element, container);
    });

    it('should render', function(){
        expect(component.getDOMNode()).toExist();
    });
});
