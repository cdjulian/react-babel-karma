// set fixtures
jasmine.getFixtures().fixturesPath = 'base/script/spec/fixtures';

// programatically dispatch mouse events
function dispatchMouseEvent(el, type, x, y) {
    if(typeof MouseEvent == 'function') {
        var e = new MouseEvent(type, {
            bubbles: true,
            cancelable: true,
            view: window,
            screenX: x,
            screenY: y,
            clientX: x,
            clientY: y
        });
        el.dispatchEvent(e);

    } else {
        var e2 = document.createEvent("MouseEvents");
        e2.initMouseEvent(type, true, true, window, 0, x, y, x, y, false, false, false, false, 0, null);
        el.dispatchEvent(e2);
    }
}