var glob = require("glob");

module.exports = {
    devtool: 'source-map',

    entry: {
        index: './script/jsx/index.jsx',
        react: glob.sync('./script/react/jsx/*.jsx'),
        spec: glob.sync('./script/spec/*.js')
    },
    output: {
        path: __dirname + '/script/dist',
        filename: '[name].bundle.js'
    },
    module: {
        preLoaders: [
            {
                test: /(\.jsx$|\.js$)/,
                loader: 'jsxhint-loader'
            }
        ],

        loaders: [
            {
                test: /(\.jsx$|\.js$)/,
                loader: 'babel-loader'
            }
        ]
    },

    jshint: {
        esnext: true
    }
};